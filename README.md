# Using CGO for PRVHASH

## Requirements

1. Install [golang >1.22](https://go.dev/)


2. Clone source library С/C++ in the ./prvhash directory

```bash
git clone https://github.com/avaneev/prvhash
```

## Run application

```bash
go run main.go
```

### Output

```output
Hash: ab429f188efdfb708cdfa887855c77da0c3fd8b95d6fe9d0bcb3e456c6a66312
```

## Benchmark

```bash
go test -bench=.
```

```output
goos: linux
goarch: amd64
pkg: prvhash
cpu: 12th Gen Intel(R) Core(TM) i3-12100F
BenchmarkHashMessage16-8        10085766               117.1 ns/op
BenchmarkHashMessage64-8        11662588                99.95 ns/op
PASS
ok      gitlab.com/manazoid/cgo-prvhash 2.577s

```

## License

MIT

## Author

Kirill Bogomolov