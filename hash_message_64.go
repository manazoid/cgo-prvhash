package main

/*
#include "./prvhash/prvhash64.h"
*/
import "C"
import "unsafe"

func HashMessage64(message string, hashLen int, useSeed interface{}) []byte {
	msgBytes := []byte(message)
	msgLen := C.size_t(len(msgBytes))

	hashLenC := C.size_t(hashLen)
	hash := make([]byte, hashLenC)

	var seed C.PRH64_T
	switch v := useSeed.(type) {
	case int:
		seed = C.PRH64_T(v)
	case int64:
		seed = C.PRH64_T(v)
	case float64:
		seed = C.PRH64_T(v)
	default:
		panic("useSeed must be int, int64, or float64")
	}

	C.prvhash64(
		unsafe.Pointer(&msgBytes[0]),
		msgLen,
		unsafe.Pointer(&hash[0]),
		hashLenC,
		seed,
	)

	return hash
}
