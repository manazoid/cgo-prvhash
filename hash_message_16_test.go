package main

import (
	"testing"
)

func BenchmarkHashMessage16(b *testing.B) {
	message := "Hello, World!"
	hashLen := 16
	useSeed := 0

	for i := 0; i < b.N; i++ {
		HashMessage16(message, hashLen, useSeed)
	}
}
