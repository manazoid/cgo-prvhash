package main

/*
#include "./prvhash/prvhash16.h"
*/
import "C"
import (
	"unsafe"
)

func HashMessage16(message string, hashLen int, useSeed interface{}) []byte {
	msgBytes := []byte(message)
	msgLen := C.size_t(len(msgBytes))

	hashLenC := C.size_t(hashLen)
	hash := make([]byte, hashLenC)

	var seed C.uint32_t
	switch v := useSeed.(type) {
	case int:
		seed = C.uint32_t(v)
	case int64:
		seed = C.uint32_t(v)
	case float64:
		seed = C.uint32_t(v)
	default:
		panic("useSeed must be int, int64, or float64")
	}

	C.prvhash16(
		unsafe.Pointer(&msgBytes[0]),
		msgLen,
		unsafe.Pointer(&hash[0]),
		hashLenC,
		seed,
	)

	return hash
}
