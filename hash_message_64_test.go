package main

import (
	"testing"
)

func BenchmarkHashMessage64(b *testing.B) {
	message := "Hello, World!"
	hashLen := 32
	useSeed := 0

	for i := 0; i < b.N; i++ {
		HashMessage64(message, hashLen, useSeed)
	}
}
