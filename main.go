package main

import "C"
import (
	"fmt"
)

func main() {
	message := "Hello, World!"
	hashLen := 32
	useSeed := 0

	hash := HashMessage64(message, hashLen, useSeed)

	fmt.Printf("Hash: %x\n", hash)
}
